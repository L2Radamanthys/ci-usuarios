<h3>Nuevo Usuario</h3>
<form action="" method="POST">
<table  border="0px" style="margin:0px auto;">
    <tr>
        <td><label>Nombre Usuario:</label></td>
        <td><input type="text" name="username" value="{username}" class="edt_c" /></td>
    </tr>
    
    <tr>
        <td colspan="2">
        {err_username}
        </td>
    </tr>
    
    <tr>
        <td><label>Contraseña:</label></td>
        <td><input type="password" name="password" class="edt_c" /></td>
    </tr>
    
    <tr>
        <td colspan="2">
        {err_password}
        </td>
    </tr>
    
    <tr>
        <td><label>Repetir Contraseña:</label></td>
        <td><input type="password" name="re_password" class="edt_c" /></td>
    </tr>
    
    <tr>
        <td colspan="2">
        {err_re_password}
        </td>
    </tr>
    
    <tr>
        <td><label>Nombre:</label></td>
        <td><input type="text" name="first-name" class="edt_m" /></td>
    </tr>
    
    <tr>
        <td><label>Apellido:</label></td>
        <td><input type="text" name="last-name" class="edt_m" /></td>
    </tr>
    
    <tr>
        <td><label>Email:</label></td>
        <td><input type="text" name="email" class="edt_m" /></td>
    </tr>
    
    <tr>
        <td><label>Grupo:</label></td>
        <td>
        <select name="group_id">
        {groups} 
            <option value="{id}">{name}</option>          
        {/groups}            
        </select>
        </td>
    </tr>
    
    <tr>
        <td colspan="2">
        <br />
        </td>
    </tr>
    
    <tr>
        <td colspan="2" align="center">
        <input type="submit" value="Crear" class="boton"/>
        <input type="reset" value="Limpiar" class="boton"/>
        </td>
    </tr>
</table>
</form>