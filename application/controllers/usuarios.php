<?php


class Usuarios extends CI_Controller {

    
    public function index()
    {
        //dicionario base aplicacion        
        $dict = array(
            'base_url' => base_url(),
            'page-header' => 'Index', 
            'page-style' => '',             //CSS Includes
            'page-javascript' => '',        //JavaScript Includes
            'page-menu' => load_menu(),     //carga el menu dependiendo el usuario
            'page-body' => '',
        );
        
        if ($this->usuarios_model->is_login()) {
            $dict['page-body'] = '<p>Usuario: <strong>'.$this->usuarios_model->get_username().'</strong></p>';
        }
        
        else {
            $dict['page-body'] = '<p>No login</p>';
        }
        
        $this->parser->parse('template', $dict);
        
    }
    
    
    public function panel() 
    {
        
        
        
    }
    
    
    /*
     * Controlador para la vista de inicio de session de usuario
     * 
     */     
    public function login() 
    {
        //basic data dict load
        $dict = generate_basic_dict();
        $dict['page-header'] = 'Inciar Session';
        
        //ya esta logueado?
        if (! $this->usuarios_model->is_login()) {
            if ($this->input->post('username') != NULL) { //se envio el form (osea hicieron comit)?
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $result = $this->usuarios_model->login($username, $password);

                if ($result) {  //login exitoso?
                    $dict['page-body'] = "<p>Session Iniciada Correctamente..</p>";
                }
                
                else {
                    $dict['page-body'] = $this->load->view('session/login', NULL, TRUE);
                    $dict['salida'] = '<p>login error</p>';
                } 
            }
            
            else {
                $dict['page-body'] = $this->load->view('session/login', NULL, TRUE);
                $dict['salida'] = '';
            }
       }
        
       else {
	       $dict['page-body'] = "<p>Error ya iniciaste session con otro usuario..</p>";
       }
        
        
        $this->parser->parse('template', $dict); 
    }
    
    
    
    
    /*
     * Vista que cierra la session de usuario existente
     * 
     */
    public function logout()
    {
        //basic data dict load
        $dict = generate_basic_dict();
        $dict['page-header'] = 'Cerrar Session';
        $dict['page-body'] = "<p>Has cerrado session exitosamente..</p>";
        $this->usuarios_model->logout();
        $this->parser->parse('template', $dict);  
    }    
    
    
    public function register() 
    {
        $this->load->model('groups_model');
        $this->load->library('form_validator');
        $dict = generate_basic_dict();
        
        $dict['err_username'] = '';
        $dict['err_password'] = '';
        
        if ($this->usuarios_model->has_access(current_url())) 
        {
            //se enviaron datos
            if ($this->input->post('username') != NULL) 
            {

                $argv = array(
                    'username' => 'NOT-NULL',
                    'password' => array('EVAL' => 'RANG_LEN', 'MIN' => 6, 'MAX' => 16),
                    're_password' => array('EVAL'=>'EQ_PSWD', 'CMP'=>'password'),
                );
                
                $this->form_validator->validate($argv);                
                
            
                //pregunta si son validos los datos del formulario
                if ($this->form_validator->is_valid()) 
                {
                    $dict['page-body'] = "Usuarios registrado correctamente";
                }
                else 
                {
                    $dict['page-body'] = $this->parser->parse('session/new-user', array('groups' => $this->groups_model->all()), TRUE);
                    $dict['page_body'] .= "<br /><br /> Error Campos Invalidos";    
                }
                
            }
            else {
                $cont = $this->parser->parse('session/new-user', array('groups' => $this->groups_model->all()), TRUE);
                $dict['page-body'] = clear_template($cont);
                #$dict['page-body'] = $cont;
                
                
            }
        }
        
        else 
        {
	       $dict['page-body'] = "Error no tienes permisos para accceder a <br /> ".current_url();
        } 
        
        
        
        $this->parser->parse('template', $dict);
    }
    

    
    
}


?>