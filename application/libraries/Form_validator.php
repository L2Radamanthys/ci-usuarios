<?php


/**
 * Clase validadora personalizada de formularios, en realidad valida
 */ 
class Form_validator {
    protected $ci;
    protected $messages; 
    var $ext;
    var $errors; //listado de errores
    
    
    
    /**
     * Constructor de la Clase, inicializador de variables
     */ 
    function __construct() {
        $this->ci =& get_instance();
        $this->ext = 'err_'; //extencion para los mensajes de error
        $this->errors = array(); 
        $this->errors['result'] = TRUE; //por defecto no habra errores
        $this->messages = array(
            'NOT-NULL' => 'Error: El Campo no puede estar vacio ',
            'IS-NUM' => 'Error: El Campo no contiene un valor numerico',
            'EMAIL' => 'No es un Email Valdo',
            'COMPARE' => 'Los Campos No Coinciden',
            'PASSWORD' => 'Las Contrasenia No Coinciden', //iden compare
            'RANG-LEN' => 'La longitud del Campo {field} no es valida' 
        
        ); 
    }
    
    
    /**
     * no es nulo
     * 
     * Evalua si el contenido del parametro es nulo tomese en cuenta 
     * que este metodo evaluara tanto como si la cadena pasada como parametro 
     * es nula, como asi si estara vacia    
     * 
     * @param string
     * @return bool
     */
    public function not_null($key) 
    {
        if ($this->ci->input->post($key) != NULL && $this->ci->input->post($key) != "")
        {
            return TRUE;
        }
        else 
        {               
            return FALSE;
        } 
    }
    
    
    /**
     * Compara si los campos de las contrasenias coinciden
     * 
     * @param string
     * @param string
     * @return bool
     * 
     */
    public function password($field_a, $field_b) 
    {
        if ($this->ci->input->post($field_a) == $this->ci->input->post($field_b))
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }
    }
    
    
    
    /**
     * Valida el formulario
     * 
     * comprueba los parametros pasados, de acuerdo a metodos de validacion
     * 
     * 
     * @param array
     * @param array
     * @param bool
     * @return array
     */ 
    public function validate($argv, $labels=NULL, $escape=TRUE) {
        $this->errors['result'] = TRUE; //reseteo
        
        foreach ($argv as $key => $value) {
            //analisis via parametro unico
            if (is_string($value)) 
            {
                switch ($value) 
                {
                    case 'NOT-NULL':
                        if (!$this->not_null($key)) 
                        {
                            $this->errors['result'] = FALSE;
                            $this->errors[$this->ext.$key] = $this->messages['NOT-NULL'];                          
                        }                        
                        break;
                        
                    case 'IS-NUM':       
                        echo "no implementado";            
                        break;
                      
                        
                    default:
                        break;
                }
            }
            
            //multi parametros
            else 
            {
                switch($value['EVAL']) 
                {
                    case 'PASSWORD': //compara si coinciden los campos
                        if ($this->password($key, $value['CMP']))
                        {
                            $this->errors['result'] = FALSE;
                            $this->errors[$this->ext.$key] = $this->messages['PASSWORD'];
                        }
                        break;                       
                        
                    default:
                        break;
                }
                                
            }
        }
        
        return $this->errors;
    }
    
    
    /**
     * Devuelve si los datos del formulario analizados son validos
     * 
     * @return bool    
     */
    public function is_valid() {
         return $this->errors['result'];      
    } 
    
}



?>