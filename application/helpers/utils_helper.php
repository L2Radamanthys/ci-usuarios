<?php


/*
 * Avandonada, use current_url() del helper url
 */ 
function my_current_url()
{
    return base_url().uri_string();
}




    

function my_url($key) {
    $URL_DICT = array (
        'user-login' => base_url().'index.php/usuarios/login',    
        'user-logout' => base_url().'index.php/usuarios/logout',
        'user-panel' => base_url().'index.php/usuarios',
        'user-register' => base_url().'index.php/usuarios/register',
    );

    return $URL_DICT[$key];    
}



/*
 * Carga el Menu Segun el tipo de usuario que se encuentre logueado
 * el tipo de usuario, es definido en variable de session 'type_user'
 * en caso de no estar definida se asume que el usuario no se encuentra
 * logueado.
 */
function load_menu() {
    $_ci =& get_instance();
    
    $cont = '<li><a href="'.base_url().'">Index</a></li>';
    $cont .= '<li><a href="'.my_url('user-panel').'">Panel</a></li>';
    $cont .= '<li><a href="'.my_url('user-register').'">Registrar Usuario</a></li>';          
    $cont .= '<li><a href="'.my_url('user-login').'">Iniciar Session</a></li>';
    $cont .= '<li><a href="'.my_url('user-logout').'">Cerrar Session</a></li>';
    return $cont;
}




function generate_basic_dict() {
    $dict = array(
        'base_url' => base_url(),
        'page-header' => '', 
        'page-style' => '',             //CSS Includes
        'page-javascript' => '',        //JavaScript Includes
        'page-menu' => load_menu(),     //carga el menu dependiendo el usuario
        'page-body' => '',
    );
    return $dict;
}


/**
 * Limpia las etiquetas en las plantillas del tipo {etiqueta}
 * 
 */
function clear_template($text, $l_delim='{', $r_delim='}')
{
    $regex = '/'.$l_delim.'(\w+)'.$r_delim.'/';
    $text = preg_replace($regex, '', $text);
    return $text;
}



?>