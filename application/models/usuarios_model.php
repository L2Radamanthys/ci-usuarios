<?php


class Usuarios_model extends CI_Model {

    /*
     * Metodo para autenticar los Usuarios mediante su nombre de usuario y contrasenia
     * 
     */
    public function login($username, $password)
    {
        $this->db->flush_cache(); //limpia la cache
        $this->db->start_cache(); //inicia la cache
        $this->db->from('Users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        if ($this->db->count_all_results() == 1) 
        {
            $query = $this->db->get();
            $list = $query->result_array();
            $user_data = array(
                'login_username' => $list[0]['username']
            );
            $this->session->set_userdata($user_data);
            return TRUE;
        }
        else 
        {
            return FALSE;                
        }       
    }    
    
    
    /*
     * Consulta si el usuario se encuentra con una session iniciada, en caso de ser afirmativo
     * ademas devolvera el nombre usuario que tiene activa la session actual
     * 
     * @return BOOLEAN
     * 
     */
    public function is_login() 
    {
        $login_username = $this->session->userdata('login_username');
        if ($login_username != NULL) 
        {
            return TRUE;
        }
        else 
        {
            return FALSE;
        }
    }
    
    
    public function register($data) 
    {
        #$query = $this->db->insert('usuarios', $data);
        
    }
    
    
    /*
     * Retorna el usuario actualmente que ha iniciado session en caso contrario retornara un
     * valor NULL
     */ 
    public function get_username() 
    {
        return $this->session->userdata('login_username');
    }
    
    
    /*
     * retorna True si el usuario tiene permiso de acceder a dicha URL
     */ 
    public function has_access($url)
    {
        //no implementada
        $debug = TRUE;
        if ($debug) {
            return TRUE;
        }
        
        else {
            return FALSE;
        }
        
    }


    /*
     * Borra los datos de session del usuario
     * 
     */
    public function logout()
    {
        $this->session->sess_destroy();
    }
   
    
    
    public function get_info($username) 
    {
        
    }
}


//?d>